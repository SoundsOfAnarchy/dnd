# DnD - project where dreams come true

- [DnD - project where dreams come true](#DnD---project-where-dreams-come-true)
- [Стиль кода](#Стиль-кода)
  - [Объявления блоков import](#Объявления-блоков-import)
- [Authors](#Authors)
  - [Mihaylov Artem](#Mihaylov-Artem)

# Стиль кода
## Объявления блоков import
  Является обязательным, если flake8 выдает вам кучу непонятных ошибок с кодом `I***`, значит перед запуском линтера вы не отсортировали импорты. Для этого запустите команду из `Makefile` `make sort-imports`, скрипт применит правила по объявлению import блоков, отсортирует и отформатирует код по правилам указанным в файле концфигурации `.isort.cfg`
  
  Файлы, которые игнорируются при форматировании можно посмотреть в полях `skip` и `skip_glob`. [Подробнее о параметрах конфигурации](https://pycqa.github.io/isort/docs/configuration/options.html)

  Если вы используете VS Code для автоматической сортировки при сохранении файла, в настройках редактора впишите следующее:

    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },

## Authors
#### 1. **Mihaylov Artem** - ci/cd, architecture, backend. [![telegram](https://icons.iconarchive.com/icons/alecive/flatwoken/24/Apps-Telegram-icon.png "Telegram")](https://t.me/ez_buckets "Telegram")
