# run all test
.PHONY: test
test:
	python3 -m pytest tests

# run sorting imports
.PHONY: sort-imports
sort-imports:
	isort .

help:
	@echo sort-imports - to automatically sort imports
