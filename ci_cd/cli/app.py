import typer

from ci_cd.cli.clients.gitlab import Gitlab
from ci_cd.cli.jobs_scripts.clean import registry
from ci_cd.cli.variables import CI_PROJECT_ID, CI_PROJECT_PATH, CI_REGISTRY

app = typer.Typer()


# TODO: почему-то это работает только когда есть команда пустышка??
@app.command()
def test(hello: str = typer.Option):
    print(hello)


@app.command()
def clean_registry(
        tag_name: str = typer.Option(default=""),
        image_name: str = typer.Option(default=""),
        project_id: str = typer.Option(default=CI_PROJECT_ID, help="Default: CI_PROJECT_ID"),
        tags_count: bool = typer.Option(default=True, help="Count of tags in registry"),
        tags: bool = typer.Option(default=True, help="List of tags in registry"),
        project_path: str = typer.Option(default=CI_PROJECT_PATH, help="Default: CI_PROJECT_PATH"),
        registry_url: str = typer.Option(default=CI_REGISTRY, help="Default: CI_REGISTRY"),
):
    gl_client = Gitlab(project_id=project_id)
    c = registry.RegistryCleaner(
        gl_client=gl_client,
        image_name=image_name,
        tag_name=tag_name,
        tags_count=tags_count,
        tags=tags,
        project_path=project_path,
        registry_url=registry_url,
    )
    c.run()


if __name__ == "__main__":
    app()
