import os

# Loguru
CI_LOGGING_LEVEL = os.getenv("CI_LOGGING_LEVEL", "INFO")

# Gitlab
GITLAB_API_TOKEN = os.getenv("GITLAB_API_TOKEN", "")
CI_PROJECT_ID = os.getenv("CI_PROJECT_ID", "")
CI_PROJECT_PATH = os.getenv("CI_PROJECT_PATH", "")
CI_REGISTRY = os.getenv("CI_REGISTRY", "")
