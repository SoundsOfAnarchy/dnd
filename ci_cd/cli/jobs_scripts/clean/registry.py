
import sys

from loguru import logger

from ci_cd.cli.clients.gitlab import Gitlab
from ci_cd.cli.logger import logger as log


class RegistryCleaner:
    """
    Clean images from dockerhub registry
    """

    def __init__(self,
                 gl_client: Gitlab,
                 tags: bool,
                 tags_count: bool,
                 tag_name: str,
                 image_name: str,
                 project_path: str,
                 registry_url: str,
                 ) -> None:
        self.__gl_client = gl_client
        self.tags = tags
        self.tags_count = tags_count
        self.tag_name = tag_name
        self.image_name = image_name
        self.project_path = project_path
        self.registry_url = registry_url

    @logger.catch
    def run(self):
        log.info(f"Start deleting tag '{self.tag_name}'...")
        try:
            log.info("Getting registry repositories for project...")
            repo_registrys = self.__gl_client.get_registry_repository(tags=self.tags,
                                                                      tags_count=self.tags_count)
        except Exception as ex:
            log.error(f"Can't get repository registry. Error: {ex}")
            sys.exit(1)

        log.echo(f"Successfully got registries '{repo_registrys}'")

        res_registry = None
        for repo_registry in repo_registrys:
            if repo_registry.get("name") != self.image_name:
                continue
            for tag in repo_registry.get("tags"):
                if tag.get("name") == self.tag_name:
                    res_registry = repo_registry
                    break

        if not res_registry:
            log.warning(f"Can't find registry for project '{self.project_path}'")
            sys.exit(0)

        try:
            log.info(f"Trying to delete tag '{self.tag_name}...'")
            ok = self.__gl_client.delete_registry_repository_tag(repository_id=res_registry.get("id"),
                                                                 tag_name=self.tag_name)
        except Exception as ex:
            log.error(f"Can't delete tag '{self.tag_name}'. Error: {ex}")
            sys.exit(1)

        if not ok:
            log.warning(f"Can't find tag '{self.tag_name}'")
            sys.exit(0)

        log.info(f"Successfully image {self.registry_url + '/' + self.project_path + '/' + self.tag_name} deleted")
