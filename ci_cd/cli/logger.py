import sys
import urllib.parse

import click
import loguru

from ci_cd.cli.variables import CI_LOGGING_LEVEL


class Logger:
    def __init__(self):
        self.__logger = loguru.logger
        self.__logger.remove()
        self.__logger.level("INFO", color="<green>")
        self.__logger.add(sys.stdout,
                          level=CI_LOGGING_LEVEL,
                          format="<level>{time} {level} {message}</level>",
                          colorize=True)

    def info(self, message: str) -> None:
        self.__logger.info(message)

    def warning(self, message: str) -> None:
        self.__logger.warning(message)

    def debug(self, message: str) -> None:
        self.__logger.debug(message)

    def error(self, message: str) -> None:
        self.__logger.error(message)

    def echo(self, message: str, color: bool = False) -> None:
        res = ""
        paint = 'red' if color else 'reset'
        for line in str(message).splitlines():
            for word in line.split(' '):
                if not word:
                    res += ' '
                    continue
                if self.__url_validator(word):
                    res += click.style(f"{word} ", fg='blue')
                else:
                    res += click.style(f"{word} ", fg=paint)
            res += '\n'
        res = res.rstrip('\n')
        click.echo(res, color=True)

    @staticmethod
    def __url_validator(string: str) -> bool:
        try:
            result = urllib.parse.urlparse(string)
            return all([result.scheme, result.netloc])
        except Exception:
            return False


logger = Logger()
