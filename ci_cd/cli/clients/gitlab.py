import pprint

import requests

from ci_cd.cli.logger import logger
from ci_cd.cli.variables import GITLAB_API_TOKEN


class Gitlab:
    API_URL = "https://gitlab.com/api/v4"

    def __init__(self, project_id: str):
        self.project_id = project_id
        self.headers = {
            "PRIVATE-TOKEN": GITLAB_API_TOKEN
        }

    def get_registry_repository(self, tags: bool, tags_count: bool) -> dict:
        url = f"{self.API_URL}/projects/{self.project_id}/registry/repositories"
        params = {
            "tags": "true" if tags else "false",
            "tags_count": "true" if tags_count else "false",
        }
        logger.debug(f"Trying GET request for url {url} and params '{params}'")
        resp = requests.get(url=url, params=params, headers=self.headers)
        if resp.status_code == 404:
            logger.debug(f"Can't find registry for project id '{self.project_id}'")
            return {}
        resp.raise_for_status()
        logger.debug(f"Got response {pprint.pformat(resp.json())}")
        return resp.json()

    def delete_registry_repository_tag(self, repository_id: str, tag_name: str) -> bool:
        url = f"{self.API_URL}/projects/{self.project_id}/registry/repositories/{repository_id}/tags/{tag_name}"
        logger.debug(f"Trying DELETE request for url {url}")
        resp = requests.delete(url=url, headers=self.headers)
        if resp.status_code == 404:
            logger.debug(f"Can't find tag '{tag_name}' with {url}")
            return False
        resp.raise_for_status()
        logger.debug(f"Successfully delete tag '{tag_name}'")
        return True
