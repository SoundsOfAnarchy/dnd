Для локального запуска экспортируйте переменные из .env файла с помощью команды:

`export $(grep -v '^#' .env | xargs)`

Пример `.env` файла:

<pre><code>DJANGO_SECRET_KEY="secret-key"
POSTGRES_USER="postgres"
POSTGRES_DB_NAME="postgres"
POSTGRES_PASSWORD="postgres-pass"
POSTGRES_HOST="db-host"
POSTGRES_PORT="5432"
</code></pre>
