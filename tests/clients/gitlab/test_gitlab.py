import pytest
import pytest_mock
import requests

from ci_cd.cli.clients.gitlab import Gitlab


def mock_client(mocker: pytest_mock.MockerFixture,
                project_id: str = "123") -> Gitlab:
    return Gitlab(project_id=project_id)


def test_get_registry_repository(mocker: pytest_mock.MockerFixture) -> None:
    e_log = mocker.patch("ci_cd.cli.logger.logger.debug")
    # test 1 - Gitlab 503
    c = mock_client(mocker)
    resp = requests.Response
    resp.status_code = 503
    mocker.patch("requests.get", return_value=resp)
    with pytest.raises(Exception):
        c.get_registry_repository(tags=True, tags_count=True)
    e_log.reset_mock()
    # test 2 - Not found registry
    c = mock_client(mocker)
    resp = requests.Response
    resp.status_code = 404
    mocker.patch("requests.get", return_value=resp)
    res = c.get_registry_repository(tags=True, tags_count=True)
    assert not res
    e_log.assert_called_with(f"Can't find registry for project id '{c.project_id}'")
    e_log.reset_mock()
    # test 3 All good
    c = mock_client(mocker)
    resp = requests.Response()
    resp.status_code = 200
    mocker.patch("requests.get", return_value=resp)
    mocker.patch.object(resp, "json", return_value={"data": "ok"})
    res = c.get_registry_repository(tags=True, tags_count=True)
    assert res.get("data") == "ok"
