# syntax=docker/dockerfile:1
FROM ubuntu:20.04

RUN apt-get update -y && apt-get -y install \
python3.8 python3-pip libpq-dev libpq-dev python-dev wget

RUN apt-get install -y --no-install-recommends build-essential gcc

# installing HELM
RUN wget https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz
RUN tar -zxvf helm-v3.8.0-linux-amd64.tar.gz
RUN mv linux-amd64/helm /usr/local/bin/helm

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ci_cd .
